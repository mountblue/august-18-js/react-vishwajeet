
import React from 'react';
import { Provider } from 'react-redux';
import Fuse from 'fuse.js';
import store from './store';
import Search from './Search';
import Footer from './Footer';
import HospitalList from './Hospital/HospitalList';
import '../style/style.css';
import HospitalData from '../services/hospital.json';


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cardData: HospitalData,
    };
    this.addData = this.addData.bind(this);
  }

  addData(input) {
    if (input.length === 0) {
      this.setState({
        cardData: HospitalData,
      });
    } else {
      const options = {
        keys: ['name'],
      };

      const fuse = new Fuse(HospitalData, options);
      const temp = fuse.search(input);
      this.setState({
        cardData: temp,
      });
    }
  }

  render() {
    return (
      <Provider store={store}>
        <div>

          <section>
            <Search addContent={this.addData} />
          </section>
          <div>
            <section>
              <HospitalList hospital={this.state.cardData} />
            </section>
          </div>
          <section className="footer">
            <Footer />
          </section>
        </div>
      </Provider>
    );
  }
}

export default App;
