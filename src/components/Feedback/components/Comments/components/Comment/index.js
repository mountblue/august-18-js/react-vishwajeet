import React from 'react';
import StarRatingComponent from 'react-star-rating-component';
import moment from 'moment';
import { Timeline, Rate } from 'antd';

import './styles.css';

const Comment = props => (
	<Timeline.Item >
		<div className="info">

         <StarRatingComponent 
          name="rate2" 
          editing={false}
          starCount={5}
          value={props.rating}
        />
			{/* <Rate
				disabled
				value={props.rating}
			/> */}
			<a href={`mailto:${props.email}`}>{props.name}</a>{' - '}
			<span className="date">{moment(props.date).format('ll')}</span>
		</div>
		{props.comment}
	</Timeline.Item>
);

export default (Comment);
