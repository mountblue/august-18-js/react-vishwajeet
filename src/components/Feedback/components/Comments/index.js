import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Timeline } from 'antd';

import { getCommentsDesc } from '../../data/comments/reducer';
import Comment from './components/Comment';
import './styles.css';

class CommentsContainer extends Component {
  render() {
    return (
      <div styleName="Comments">
        <h2>
Comments(
{this.props.comments.length}
)
</h2>
        {this.props.comments.length ? (
          <Timeline>
            {this.props.comments.map((comment, index) => (
              <Comment
                key={index}
                {...comment}
              />
            ))}
          </Timeline>
        ) : 'No comments'}
      </div>
    );
  }
}

export default connect(state => ({
  comments: getCommentsDesc(state.Feedback.data.comments),
}))(CommentsContainer);
