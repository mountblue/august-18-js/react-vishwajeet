import React from 'react';
import { Layout } from 'antd';
import Form from './components/Form';
import Comments from './components/Comments';
import './style.css';

const Feedback = () => (
  <div className="Feedback">
    <Layout>
      <Layout.Header>
		Give Your Valuable Feedback
      </Layout.Header>
      <Layout.Content>
        <Form />
        <Comments />
      </Layout.Content>
    </Layout>
  </div>
);

export default Feedback;
