import { combineReducers } from 'redux';

import { reducer as dataReducer } from './data/reducer';

export const reducer = combineReducers({
	data: dataReducer,
});



/* const initialState = {
    a:1,
    b:1
};

const reducer = (state = initialState,action)=>{
   
    switch(action.type){
case 'UPDATE_A':
return{
...state,
a: state.a+state.b
}
case 'UPDATE_B':
return{
...state,

b: state.b+state.a
}

case 'AGE_DOWN':
return{
    ...state,
age: state.age-action.value,
history: state.history.concat({age: state.age-action.value})

}
default:
return state;
    }
   
};

export default reducer; */