import React from 'react';

class Footer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <footer>
        Created by
        {'   '}
        <a href="https://www.linkedin.com/in/vishwajeet-raj/" rel="noopener noreferrer" target="_blank" title="Linkedin Profile"> Vishwajeet Raj</a>
      </footer>
    );
  }
}


export default Footer;
