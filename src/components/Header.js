import React, { Component } from 'react';

import './Header.css';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <nav>
        <div className="header">
          <section className="logo">
            <img src="../src/images/practo.png" alt="nothing Found" id="practo-logo" />
          </section>

          <section className="header-links">
            <div className="links"><a href="#">Book Appointment</a></div>
            <div className="links"><a href="#">Order Medicines</a></div>
            <div className="links"><a href="#">Book tests and Checkups</a></div>
            <div className="links"><a href="#">Chat with doctors</a></div>
          </section>
        </div>
      </nav>
    );
  }
}
export default Header;
