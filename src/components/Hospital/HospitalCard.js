import React from 'react';
import PropTypes from 'prop-types';
import { Rate, Badge } from 'antd';
import './HospitalCard.css';
import {
  Card, CardHeader, CardBody, CardFooter, ImageHeader,
} from 'react-simple-card';
import StarRating from '../StarRating';

class HospitalCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    // this.myClick = this.myClick.bind(this)
  }

  // myClick() {
  //   console.log('MyClick');
  // }

  render() {
    return (
      <div className="card">
        <Card>
          <ImageHeader imageSrc={this.props.hospital.url} />

          <CardHeader>{this.props.hospital.name}</CardHeader>

          <CardBody>
            <h5>
              {this.props.hospital.locality}
,
              {this.props.hospital.city}
            </h5>

          </CardBody>
          <CardFooter>
            {/* <StarRating rating={this.props.hospital.rating} />
 */}
            <div>
              <Rate defaultValue={this.props.hospital.rating} disabled allowHalf />
            </div>
          </CardFooter>
        </Card>

      </div>
    );
    // );
  }
}

HospitalCard.defaultProps = {
  hospital: {},
};

HospitalCard.propTypes = {
  hospital: PropTypes.object,
};


export default HospitalCard;
