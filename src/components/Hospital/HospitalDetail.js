import React from 'react';
import { Provider } from 'react-redux';
import { Rate, Badge } from 'antd';
import store from '../store';
import SlideShow from './SlideShow';
import Footer from '../Footer';
import HospitalData from '../../services/hospital.json';
import Feedback from '../Feedback';
import './HospitalCard.css';

class HospitalDetails extends React.Component {
  constructor(props) {
    super();
    this.state = {
      data: HospitalData.filter(obj => obj.key === props.username)[0],
    };
  }


  render() {
    return (
      <Provider store={store}>
        <div>
          <h3 className="headindDetails">{this.state.data.name}</h3>
          <div className="details">
            <div className="sec">
              <SlideShow sampleImage={this.state.data.fadeImages} />
            </div>
            <div className="detailsSection">

              <div>
                <h3>
              Name :
                  {' '}
                  {this.state.data.name}
                </h3>
                <h4>
              Location :
                  {' '}
                  {this.state.data.locality}
                </h4>
                <h4>Type : Hospital</h4>
                <h5>
              City :
                  {' '}
                  {this.state.data.city}
                , India
                </h5>

              </div>
              <div>
<Rate defaultValue={this.state.data.rating} disabled allowHalf />
                <Badge count={109} style={{ backgroundColor: '#87d068' }} />

              </div>
            </div>
          </div>
          <div>
            <Feedback />
          </div>
          <section className="footer">
            <Footer />
          </section>
        </div>
      </Provider>

    );
  }
}

export default HospitalDetails;
