import React from 'react';
import { Link } from 'react-router-dom';
// import Route from 'react-router-dom/Route';
import PropTypes from 'prop-types';
import HospitalCard from './HospitalCard';
import './HospitalCard.css';

/* const click = (eve) => {
  console.log(`hellWo${eve}`);
    <Link to="/hello/Manipal" />;
    onClick={click.bind(this, data.key)}
}; */

const gethospital = (hospital) => {
  if (hospital.length === 0) {
    return (
      <div className="noResult">
        <h4>Sorry No Result Found</h4>
      </div>
    );
  }


  return (
    <div className="list">
      {
         hospital.map(data => (
           <div key={data.key}>
             <Link to={`/${data.name}/${data.key}`}>
               <HospitalCard hospital={data} />
             </Link>
           </div>
         ))
            }
    </div>
  );
};

const HospitalList = props => (
  <div>
    {gethospital(props.hospital)}
  </div>
);

HospitalList.defaultProps = {
  hospital: [],
};

HospitalList.propTypes = {
  hospital: PropTypes.array,
};

export default HospitalList;
