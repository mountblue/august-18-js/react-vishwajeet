import React from 'react';
import { Fade } from 'react-slideshow-image';
import './HospitalCard.css';

const fadeProperties = {
  duration: 5000,
  transitionDuration: 300,
  infinite: true,
  indicators: true,
};

const Slideshow = props => (

  <Fade {...fadeProperties}>
    {
      props.sampleImage.map((data, index) => (
        <div key={index}>
          <img className="img" src={data} alt="Not Available" />
        </div>
      ))
    }
  </Fade>

);

export default Slideshow;
