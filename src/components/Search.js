import React from 'react';
import './Search.css';

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      input: '',
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.props.addContent(event.target.value);
  }


  render() {
    return (
      <div>
        <div className="Search-title">Search Hospital</div>
        <div className="search">
          <input type="text" className="search-bar" placeholder="Search" onChange={this.handleChange} />
          <span className="fa fa-search search-icon" />
        </div>
      </div>
    );
  }
}

export default Search;
