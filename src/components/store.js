import { createStore, combineReducers } from 'redux';

import { reducer as feedbackReducer } from './Feedback/reducer';

const appReducer = combineReducers({
  Feedback: feedbackReducer,
});

export default createStore(
  appReducer,
);
