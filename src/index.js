
import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import Route from 'react-router-dom/Route';
import App from './components/App';

import Header from './components/Header';
import './style/style.css';
import HospitalDetails from './components/Hospital/HospitalDetail';


const Demo = params => (<HospitalDetails username={params.username} />);


class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // cardData: HospitalData,
    };
    // this.addData = this.addData.bind(this);
  }

  render() {
    return (
      <Router>
        <div className="mainContainer">
          <section>
            <Header />
          </section>

          <Route
            path="/"
            exact
            component={App}

          />

          <Route
            path="/:username/:id"
            exact
            render={({ match }) => <Demo username={match.params.id} />}
          />
        </div>
      </Router>
    );
  }
}

render(<Index />, document.getElementById('app'));
